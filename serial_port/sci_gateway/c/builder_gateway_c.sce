// ====================================================================
// Allan CORNET
// DIGITEO 2010
// ====================================================================

includes_src_c = '-I""' + get_absolute_file_path('builder_gateway_c.sce') + '../../src/c""';

// PutLhsVar managed by user in sci_sum and in sci_sub
// if you do not this variable, PutLhsVar is added
// in gateway generated (default mode in scilab 4.x and 5.x)
WITHOUT_AUTO_PUTLHSVAR = %t;

sci_c_functions = ['openCOM','sci_openCOM';
'closeCOM','sci_closeCOM';
'readCOM','sci_readCOM';
'writeCOM','sci_writeCOM';
'purgeCOM','sci_purgeCOM';
'statusCOM','sci_statusCOM'];

files_c_functions = ['sci_openCOM.c',
'sci_closeCOM.c',
'sci_readCOM.c',
'sci_writeCOM.c',
'sci_purgeCOM.c',
'sci_statusCOM.c'];


tbx_build_gateway('gw_serial', sci_c_functions, files_c_functions, ..
                  get_absolute_file_path('builder_gateway_c.sce'), ..
                  ['../../src/c/libserial_port'],'',includes_src_c);

clear WITHOUT_AUTO_PUTLHSVAR;

clear tbx_build_gateway;
