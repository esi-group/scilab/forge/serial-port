//=============================================================================
// Allan CORNET
// DIGITEO - 2010
// CeCill
//=============================================================================
#include <stdlib.h>
#include <string.h>

#include "ports_manager.h"
#include "serial.h"
//=============================================================================
COM_port PORTS_SERIAL[PORTS_MAX];
static BOOL bInit = FALSE;
//=============================================================================
BOOL initializeStructPorts(void)
{
	if (!bInit)
	{
		int i = 0;
		for (i = 0; i < PORTS_MAX; i++)
		{
			PORTS_SERIAL[i].g_hCOM = 0;
			PORTS_SERIAL[i].bUsed = FALSE;
			PORTS_SERIAL[i].portname[0] = '\0';
			PORTS_SERIAL[i].baudrate = DEFAULT_BAUDRATE;
			PORTS_SERIAL[i].parity = DEFAULT_PARITY;
			PORTS_SERIAL[i].protocol = DEFAULT_PROTOCOL;
			PORTS_SERIAL[i].stopBits = DEFAULT_STOPBITS;
			PORTS_SERIAL[i].byteSize = DEFAULT_BYTESIZE;
			PORTS_SERIAL[i].timeout = DEFAULT_TIMEOUT;
		}
		bInit = TRUE;
	}
	return TRUE;
}

static int getFirstUsablePort()
{
    int i = 0;
    for(i = 0; i < PORTS_MAX ; i++)
    {
        if(PORTS_SERIAL[i].bUsed == FALSE)
        {
            return i;
        }
    }

    return -1;
}

static int getPortIndex(char* _pstPortName)
{
    int i = 0;
    for(i = 0; i < PORTS_MAX ; i++)
    {
        if(strcmp(PORTS_SERIAL[i].portname, _pstPortName) == 0)
        {
            return i;
        }
    }

    return -1;
}

//=============================================================================
BOOL isUsedPort(char* _pstPortName)
{
    int id = getPortIndex(_pstPortName);

    if(id != -1)
    {
    	return PORTS_SERIAL[id].bUsed;
    }

    return FALSE;
}
//=============================================================================
BOOL isUsedPortId(int iID)
{
	if ((iID < 1) && (iID > PORTS_MAX))
	{
		return FALSE;
	}

	return PORTS_SERIAL[iID - 1].bUsed;
}
//=============================================================================

COM_port * getPortInfo(int iID)
{
	int id = 0;
	if ((iID < 1) && (iID > PORTS_MAX))
	{
		return NULL;
	}

	id = iID - 1;

	if (PORTS_SERIAL[id].bUsed)
	{
		return &(PORTS_SERIAL[id]);
	}
	return NULL;
}
//=============================================================================
HANDLE_PORT getHandleCOM(int iID)
{
	int id = 0;
	if (iID < 1 && iID > PORTS_MAX)
	{
		return 0;
	}

	id = iID - 1;

	if (PORTS_SERIAL[id].bUsed)
	{
		return PORTS_SERIAL[id].g_hCOM;
	}
	return 0;
}
//=============================================================================
int openPortCOM(char* _pstPortName, int baudrate, int bytesize, int parity, int protocol, int stopbits, int timeout)
{
	int ierr = 0;

    int id = getPortIndex(_pstPortName);
	if (id != -1 && PORTS_SERIAL[id].bUsed)
	{
		return id;
	}
	else
	{
		HANDLE_PORT hCOM = OpenCOM(_pstPortName, baudrate, parity, protocol, timeout, stopbits, bytesize, &ierr);
        id = getFirstUsablePort();

		if (ierr == 0 && id != -1)
		{
			PORTS_SERIAL[id].g_hCOM = hCOM;
			PORTS_SERIAL[id].bUsed = TRUE;
			strcpy(PORTS_SERIAL[id].portname, _pstPortName);
			PORTS_SERIAL[id].baudrate = baudrate;
			PORTS_SERIAL[id].parity = parity;
			PORTS_SERIAL[id].protocol = protocol;
			PORTS_SERIAL[id].stopBits = stopbits;
			PORTS_SERIAL[id].byteSize = bytesize;
			PORTS_SERIAL[id].timeout = timeout;
			return id + 1;
		}
        else
        {
            CloseCOM(hCOM);
        }
	}
	return -1;
}
//=============================================================================
BOOL closePortCOM(int nId)
{
	if (PORTS_SERIAL[nId - 1].bUsed)
	{
		BOOL bClosed = CloseCOM(PORTS_SERIAL[nId - 1].g_hCOM);

		PORTS_SERIAL[nId - 1].g_hCOM = 0;
		PORTS_SERIAL[nId - 1].bUsed = FALSE;
        PORTS_SERIAL[nId - 1].portname[0] = '\0';
		PORTS_SERIAL[nId - 1].baudrate = DEFAULT_BAUDRATE;
		PORTS_SERIAL[nId - 1].parity = DEFAULT_PARITY;
		PORTS_SERIAL[nId - 1].protocol = DEFAULT_PROTOCOL;
		PORTS_SERIAL[nId - 1].stopBits = DEFAULT_STOPBITS;
		PORTS_SERIAL[nId - 1].byteSize = DEFAULT_BYTESIZE;
		PORTS_SERIAL[nId - 1].timeout = DEFAULT_TIMEOUT;
		return bClosed;
	}
	return FALSE;
}
//=============================================================================
BOOL purgePortCOM(int nId)
{
	if (PORTS_SERIAL[nId - 1].bUsed)
	{
		return PurgeCOM(PORTS_SERIAL[nId - 1].g_hCOM);
	}
	return FALSE;
}
//=============================================================================
BOOL statusPortCOM(int nId, int* _piIn, int* _piOut)
{
	if (PORTS_SERIAL[nId - 1].bUsed)
	{
		return StatusCOM(_piIn, _piOut, PORTS_SERIAL[nId - 1].g_hCOM);
	}
	return FALSE;
}

//=============================================================================
BOOL writeString(int iID, char *bufferToWrite, int *BytesWritten)
{
	int id = 0;
	if (iID < 1 && iID > PORTS_MAX)
	{
		return FALSE;
	}

	*BytesWritten = 0;

	id = iID - 1;
	if (PORTS_SERIAL[id].bUsed)
	{
		return WriteCOM(bufferToWrite, (int)strlen(bufferToWrite), BytesWritten, PORTS_SERIAL[id].g_hCOM);
	}
	return FALSE;
}
//=============================================================================
BOOL writeBytes(int iID, BYTE *bufferToWrite, int lenBuffer, int *BytesWritten)
{
	int id = 0;
	if (iID < 1 && iID > PORTS_MAX)
	{
		return FALSE;
	}

	*BytesWritten = 0;

	id = iID - 1;
	if (PORTS_SERIAL[id].bUsed)
	{
		return WriteCOM(bufferToWrite, lenBuffer, BytesWritten, PORTS_SERIAL[id].g_hCOM);
	}
	return FALSE;
}
//=============================================================================
BOOL readString(int iID, char *bufferRead, int sizeToRead, int *bytesRead)
{
	int id = 0;
	if (iID < 1 && iID > PORTS_MAX)
	{
		return FALSE;
	}

	*bytesRead = 0;

	id = iID - 1;
	if (PORTS_SERIAL[id].bUsed)
	{
		return ReadCOM(bufferRead, sizeToRead, bytesRead, PORTS_SERIAL[id].g_hCOM);
	}
	return FALSE;
}
//=============================================================================
BOOL readBytes(int iID, BYTE *bufferRead, int sizeToRead, int *bytesRead)
{
	int id = 0;
	if (iID < 1 && iID > PORTS_MAX)
	{
		return FALSE;
	}

	*bytesRead = 0;

	id = iID - 1;
	if (PORTS_SERIAL[id].bUsed)
	{
		return ReadCOM(bufferRead, sizeToRead, bytesRead, PORTS_SERIAL[id].g_hCOM);
	}
	return FALSE;
}
//=============================================================================
