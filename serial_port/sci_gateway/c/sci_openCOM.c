//=============================================================================
// Allan CORNET
// DIGITEO - 2010
// CeCill
//=============================================================================
#include "ports_manager.h"
//=============================================================================
#include "api_scilab.h"
#include "localization.h"
#include "Scierror.h"
//=============================================================================
static int getValueAsIntAtRhs(void* _pvCtx, int rhs_position, char * fname, int *ierr);
//=============================================================================
int sci_openCOM(char *fname)
{
	SciErr sciErr;
	int ierr = 0;
	int bOK = (int)FALSE;

    int* piAddr = NULL;
	char* PORT = NULL;
    int iPort = 0;

	int BAUDRATE = DEFAULT_BAUDRATE;
	int BYTESIZE = DEFAULT_BYTESIZE;
	int PARITY = DEFAULT_PARITY;
	int STOPBITS = DEFAULT_STOPBITS;
	int TIMEOUT = DEFAULT_TIMEOUT;
	int PROTOCOL = DEFAULT_PROTOCOL;

	COM_port *portInfo = NULL;

	initializeStructPorts();

	CheckRhs(1,7);
	CheckLhs(1,1);

	if (Rhs > 6) /* Rhs == 7 */
	{
		TIMEOUT = getValueAsIntAtRhs(pvApiCtx, 7, fname,&ierr);
		if (ierr) return 0;
		if (!checkTimeOut(TIMEOUT))
		{
			Scierror(999,"%s: Wrong value for input argument #%d.\n", fname, 7);
			return 0;
		}
	}

	if (Rhs > 5) /* Rhs == 6 */
	{
		PROTOCOL = getValueAsIntAtRhs(pvApiCtx, 6, fname,&ierr);
		if (ierr) return 0;
		if (!checkProtocol(PROTOCOL))
		{
			Scierror(999,"%s: Wrong value for input argument #%d.\n", fname, 6);
			return 0;
		}
	}

	if (Rhs > 4) /* Rhs == 5 */
	{
		STOPBITS = getValueAsIntAtRhs(pvApiCtx, 5, fname, &ierr);
		if (ierr) return 0;
		if (!checkStopBits(STOPBITS))
		{
			Scierror(999,"%s: Wrong value for input argument #%d.\n", fname, 5);
			return 0;
		}
	}

	if (Rhs > 3) /* Rhs == 4 */
	{
		PARITY = getValueAsIntAtRhs(pvApiCtx, 4, fname, &ierr);
		if (ierr) return 0;
		if (!checkParity(PARITY))
		{
			Scierror(999,"%s: Wrong value for input argument #%d.\n", fname, 4);
			return 0;
		}
	}

	if (Rhs > 2) /* Rhs == 3 */
	{
		BYTESIZE = getValueAsIntAtRhs(pvApiCtx, 3, fname, &ierr);
		if (ierr) return 0;
		if (!checkByteSize(BYTESIZE))
		{
			Scierror(999,"%s: Wrong value for input argument #%d.\n", fname, 3);
			return 0;
		}
	}

	if (Rhs > 1) /* Rhs == 2 */
	{
		BAUDRATE = getValueAsIntAtRhs(pvApiCtx, 2, fname, &ierr);
		if (ierr) return 0;
		if (!checkBaudRate(BAUDRATE))
		{
			Scierror(999,"%s: Wrong value for input argument #%d.\n", fname, 2);
			return 0;
		}
	}

    sciErr = getVarAddressFromPosition(pvApiCtx, 1, &piAddr);
	if(sciErr.iErr)
	{
		printError(&sciErr, 0);
		return 0;
	}

	if(getAllocatedSingleString(pvApiCtx, piAddr, &PORT))
    {
        Scierror(999,_("%s: Wrong type for input argument #%d: String expected.\n"),fname,1);
        return 0;
    }

	if (isUsedPort(PORT))
	{
		Scierror(999,"%s: Port %s already opened.\n", fname, PORT);
		return 0;
	}

	iPort = openPortCOM(PORT, BAUDRATE, BYTESIZE, PARITY, PROTOCOL, STOPBITS, TIMEOUT);

    if(iPort == -1)
    {
		Scierror(999,"%s: Failed to open port %s.\n", fname, PORT);
		return 0;
    }

	createScalarDouble(pvApiCtx, Rhs + 1, iPort);
	LhsVar(1) = Rhs + 1;
    PutLhsVar();
	return 0;
}
//=============================================================================
int getValueAsIntAtRhs(void* _pvCtx, int rhs_position, char * fname, int *ierr)
{
	SciErr sciErr;
	int *piAddressVar = NULL;
	double dVar = 0;

	sciErr = getVarAddressFromPosition(_pvCtx, rhs_position, &piAddressVar);
	if(sciErr.iErr)
	{
		printError(&sciErr, 0);
		*ierr = 1;
		return 0;
	}

	if(getScalarDouble(_pvCtx, piAddressVar, &dVar))
	{
		printError(&sciErr, 0);
		Scierror(999,"%s: Wrong type for input argument #%d: A scalar expected.\n", fname, rhs_position);
		*ierr = 1;
		return 0;
	}

	*ierr = 0;
	return (int)dVar;

}
//=============================================================================
