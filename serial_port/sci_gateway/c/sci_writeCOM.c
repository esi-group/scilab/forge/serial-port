//=============================================================================
// Allan CORNET
// DIGITEO - 2010
// CeCill
//=============================================================================
#include "ports_manager.h"
//=============================================================================
#include "api_scilab.h"
#include "localization.h"
#include "Scierror.h"
#include "sciprint.h"
//=============================================================================
static BYTE* doublesToBytes(double *pDouble, int iSize);
//=============================================================================
/* [bOK, BytesWritten, BytesSended] = writeCOM(port, string or bytes vector) */
//=============================================================================
int sci_writeCOM(char *fname)
{
	SciErr sciErr;
	int m1 = 0, n1 = 0;
	int *piAddressVarOne = NULL;
	double dVarOne = 0;
	int iType1 = 0;
	int result = (int)FALSE;

	int m2 = 0, n2 = 0;
	int *piAddressVarTwo = NULL;
	double *pdVarTwo = NULL;
	int iType2 = 0;
	char *strVarTwo = NULL;
	int lenVarTwo = 0;

	int BytesInput = 0;
	int BytesWritten = 0;

	initializeStructPorts();

	CheckRhs(2,2);
	CheckLhs(1,3);

	sciErr = getVarAddressFromPosition(pvApiCtx, 1, &piAddressVarOne);
	if(sciErr.iErr)
	{
		printError(&sciErr, 0);
		return 0;
	}

	if(getScalarDouble(pvApiCtx, piAddressVarOne, &dVarOne))
	{
		printError(&sciErr, 0);
		Scierror(999,"%s: Wrong type for input argument #%d: A scalar expected.\n", fname, 1);
		return 0;
	}

	if (!isUsedPortId((int)dVarOne))
	{
		result = (int)FALSE;
		BytesInput = 0;
		BytesWritten = 0;
    }
    else
    {
		sciErr = getVarAddressFromPosition(pvApiCtx, 2, &piAddressVarTwo);
		if(sciErr.iErr)
		{
			printError(&sciErr, 0);
			return 0;
		}

		sciErr = getVarType(pvApiCtx, piAddressVarTwo, &iType2);
		if(sciErr.iErr)
		{
			printError(&sciErr, 0);
			return 0;
		}

		if ( (iType2 != sci_matrix) && (iType2 != sci_strings))
		{
			Scierror(999,"%s: Wrong type for input argument #%d: A scalar expected.\n", fname, 1);
			return 0;
		}

		if (iType2 ==  sci_matrix)
		{
			sciErr = getMatrixOfDouble(pvApiCtx, piAddressVarOne, &m2, &n2, &pdVarTwo);
			if(sciErr.iErr)
			{
				printError(&sciErr, 0);
				return 0;
			}

			if (m2 == 1 || n2 == 1)
			{
				BYTE *valuesToSend = doublesToBytes(pdVarTwo, m2 * n2);
				if (valuesToSend)
				{
					BytesInput = m2 * n2;
					if (BytesInput > DEFAULT_BUFFER_OUT_SIZE)
					{
						sciprint("WARNING: vector size is greater than %d.\n", DEFAULT_BUFFER_OUT_SIZE);
					}
					result = (BOOL)writeBytes((int)dVarOne, valuesToSend, BytesInput, &BytesWritten);
					free(valuesToSend);
					valuesToSend = NULL;
				}
			}
			else
			{
				Scierror(999,"%s: Wrong size for input argument #%d: A vector expected.\n", fname, 2);
				return 0;
			}
		}
		else
		{
			sciErr = getMatrixOfString(pvApiCtx, piAddressVarTwo, &m2, &n2, &lenVarTwo, &strVarTwo);
			if(sciErr.iErr)
			{
				printError(&sciErr, 0);
				return 0;
			}

			if (m2 != n2 && n2 != 1)
			{
				Scierror(999,"%s: Wrong size for input argument #%d: A string expected.\n", fname, 2);
				return 0;
			}

			strVarTwo = (char*)malloc(sizeof(char)*(lenVarTwo + 1));
			if (strVarTwo)
			{
				sciErr = getMatrixOfString(pvApiCtx, piAddressVarTwo, &m2, &n2, &lenVarTwo, &strVarTwo);
				if(sciErr.iErr)
				{
					printError(&sciErr, 0);
					return 0;
				}

				BytesInput = (int) strlen(strVarTwo);
				if (BytesInput > DEFAULT_BUFFER_OUT_SIZE)
				{
					sciprint("WARNING: vector size is greater than %d.\n", DEFAULT_BUFFER_OUT_SIZE);
				}

				result = (BOOL)writeString((int)dVarOne, strVarTwo, &BytesWritten);

				free(strVarTwo);
				strVarTwo = NULL;
			}
			else
			{
				Scierror(999,"%s: error memory allocation.\n", fname);
				return 0;
			}
		}
	}

	if(createScalarBoolean(pvApiCtx, Rhs + 1, result))
	{
		printError(&sciErr, 0);
		return 0;
	}

	LhsVar(1) = Rhs + 1;

	if (Lhs > 1)
	{
		if(createScalarDouble(pvApiCtx, Rhs + 2, BytesWritten))
		{
			printError(&sciErr, 0);
			return 0;
		}
		LhsVar(2) = Rhs + 2;
	}

	if (Lhs > 2)
	{
		if(createScalarDouble(pvApiCtx, Rhs + 3, BytesInput))
		{
			printError(&sciErr, 0);
			return 0;
		}
		LhsVar(3) = Rhs + 3;
	}

  PutLhsVar();

	return 0;
}
//=============================================================================
BYTE *doublesToBytes(double *pDouble,int iSize)
{
	BYTE *result = NULL;
	if (pDouble)
	{
		if (iSize > 0)
		{
			int i = 0;
			result = (BYTE*)malloc(sizeof(BYTE) * iSize);
			for (i = 0; i < iSize; i++)
			{
				result[i] = (BYTE)pDouble[i];
			}
		}
	}
	return result;
}
//=============================================================================
