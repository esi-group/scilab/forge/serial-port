//=============================================================================
// Antoine ELIAS
// Scilab enterprises 2013
// CeCill
//=============================================================================
#include "ports_manager.h"
//=============================================================================
#include "api_scilab.h"
#include "localization.h"
#include "Scierror.h"
//=============================================================================
int sci_statusCOM(char *fname)
{
	SciErr sciErr;
	int m = 0, n = 0;
	int *piAddressVar = NULL;
	double dVar = 0;
	int iType = 0;
	int result = (int)FALSE;
    int iNbIn = 0;
    int iNbOut = 0;

	CheckRhs(1,1) ;
	CheckLhs(1,3) ;

	initializeStructPorts();

	sciErr = getVarAddressFromPosition(pvApiCtx, 1, &piAddressVar);
	if(sciErr.iErr)
	{
		printError(&sciErr, 0);
		return 0;
	}

	if(getScalarDouble(pvApiCtx, piAddressVar, &dVar))
	{
		printError(&sciErr, 0);
		Scierror(999,"%s: Wrong type for input argument #%d: A scalar expected.\n", fname, 1);
		return 0;
	}

	result = statusPortCOM((int)dVar, &iNbIn, &iNbOut);

	if(createScalarBoolean(pvApiCtx, Rhs + 1, result))
	{
		printError(&sciErr, 0);
		return 0;
	}

	LhsVar(1) = Rhs + 1;

    if(Lhs > 1)
    {
	    if(createScalarDouble(pvApiCtx, Rhs + 2, iNbIn))
	    {
		    printError(&sciErr, 0);
		    return 0;
	    }
    	LhsVar(2) = Rhs + 2;
    }

    if(Lhs > 2)
    {
	    if(createScalarDouble(pvApiCtx, Rhs + 3, iNbOut))
	    {
		    printError(&sciErr, 0);
		    return 0;
	    }
    	LhsVar(3) = Rhs + 3;
    }

    PutLhsVar();

	return 0;
}
//=============================================================================
