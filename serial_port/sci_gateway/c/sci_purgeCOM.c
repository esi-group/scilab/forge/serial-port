//=============================================================================
// Allan CORNET
// DIGITEO - 2010
// CeCill
//=============================================================================
#include "ports_manager.h"
//=============================================================================
#include "api_scilab.h"
#include "localization.h"
#include "Scierror.h"
//=============================================================================
int sci_purgeCOM(char *fname)
{
	SciErr sciErr;
	int m = 0, n = 0;
	int *piAddressVar = NULL;
	double dVar = 0;
	int iType = 0;
	int result = (int)FALSE;

	initializeStructPorts();

	CheckRhs(1,1) ;
	CheckLhs(1,1) ;

	sciErr = getVarAddressFromPosition(pvApiCtx, 1, &piAddressVar);
	if(sciErr.iErr)
	{
		printError(&sciErr, 0);
		return 0;
	}

	if(getScalarDouble(pvApiCtx, piAddressVar, &dVar))
	{
		printError(&sciErr, 0);
		Scierror(999,"%s: Wrong size for input argument #%d: A scalar expected.\n", fname, 1);
		return 0;
	}

	result = purgePortCOM((int)dVar);

	sciErr = createMatrixOfBoolean(pvApiCtx, Rhs + 1, 1, 1, &result);
	if(sciErr.iErr)
	{
		printError(&sciErr, 0);
		return 0;
	}

	LhsVar(1) = Rhs + 1;
  PutLhsVar();

	return 0;
}
//=============================================================================
